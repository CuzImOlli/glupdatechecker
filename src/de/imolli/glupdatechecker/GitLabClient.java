package de.imolli.glupdatechecker;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * @author Oliver Stahl
 * @version 1.0
 */
public class GitLabClient {

    private String url;

    /**
     * @param url API URL to GitLab
     */
    public GitLabClient(String url) {
        this.url = url;
    }

    /**
     * @param request Request to send
     * @return The result (mostly JSON) as String.
     */
    public String sendGet(String request) {

        try {
            StringBuilder result = new StringBuilder();
            URL url = new URL(this.url + request);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");

            BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));

            String line;

            while ((line = rd.readLine()) != null) {
                result.append(line);
            }

            rd.close();

            return result.toString();
        } catch (IOException e) {
            e.printStackTrace();

            System.err.println(GLUpdateChecker.getPREFIX() + "Failed to send GET request.");
        }

        return "";
    }

}
