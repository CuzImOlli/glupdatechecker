package de.imolli.glupdatechecker.examples;

import de.imolli.glupdatechecker.GLUpdateChecker;
import de.imolli.glupdatechecker.UpdateResult;

public class Example {


    public static void main(String[] args) {

        GLUpdateChecker glUpdateChecker = new GLUpdateChecker("10442455", "1.7.2");
        UpdateResult result = glUpdateChecker.checkForUpdate();

        if (result.getStatus().equals(UpdateResult.Status.NEW_VERSION_AVAILABLE)) {
            System.out.println("New version available: " + result.getNewVersion());
        } else if (result.getStatus().equals(UpdateResult.Status.ALREADY_ON_NEWEST_VERSION)) {
            System.out.println("You already got the newest version!");
        } else if (result.getStatus().equals(UpdateResult.Status.FAILED)) {
            System.out.println("Failed to check version.");
        }

    }

}
