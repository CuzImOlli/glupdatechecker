package de.imolli.glupdatechecker;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * This class is used to check for new updates via GitLab Tags.
 * It connects via the {@link GitLabClient} to GitLab and load all Tags from an project. Then it checks if the newest Tag name is equal with the current version.
 * If not an new Update is available
 * <br><br>
 * Add JSON Simple as maven dependency to your pom.xml
 *
 * @author Oliver Stahl
 * @version 1.0
 * @see GitLabClient
 * @see UpdateResult
 */
public class GLUpdateChecker {

    private static final String PREFIX = "[GLUpdateChecker] ";

    private String projectID;
    private String currentVersion;
    private String remoteVersion;
    private GitLabClient gitLabClient;

    /**
     * @param projectID      GitLab projectID
     * @param currentVersion CurrentVersion of the application
     */
    public GLUpdateChecker(String projectID, String currentVersion) {
        this.projectID = projectID;
        this.currentVersion = currentVersion;

        this.gitLabClient = new GitLabClient("https://gitlab.com/api/v4/");

    }

    /**
     * Checks for an update via the {@link GitLabClient}.
     *
     * @return The Result of the UpdateCheck. See {@link UpdateResult} for more information.
     * @see UpdateResult
     */
    public UpdateResult checkForUpdate() {

        UpdateResult updateResult = new UpdateResult(UpdateResult.Status.FAILED, null);

        try {

            String resultRAW = gitLabClient.sendGet("projects/" + projectID + "/repository/tags");
            JSONArray array = (JSONArray) new JSONParser().parse(resultRAW);

            if (array.size() == 0) {

                System.err.println(PREFIX + "Failed to check for update. Couldn't find any remote versions!");

                return updateResult;
            }

            JSONObject jsonObject = (JSONObject) array.get(0);
            String version = (String) jsonObject.get("name");

            if (version != null && version.equalsIgnoreCase("")) {
                this.remoteVersion = version;
            } else {

                System.err.println(PREFIX + "Failed to read version!");

                return updateResult;
            }

            if (currentVersion.equalsIgnoreCase(version)) {
                updateResult = new UpdateResult(UpdateResult.Status.ALREADY_ON_NEWEST_VERSION, currentVersion);
            } else {
                updateResult = new UpdateResult(UpdateResult.Status.NEW_VERSION_AVAILABLE, version);
            }


        } catch (ParseException e) {
            e.printStackTrace();
            System.err.println(PREFIX + "Failed to check for update...");
        }

        return updateResult;
    }

    /**
     * @return The remote version of the project. Only available if {@link GLUpdateChecker#checkForUpdate()} is called before.
     */
    public String getRemoteVersion() {
        return remoteVersion;
    }

    /**
     * @return The projectID of the GitLab project
     */
    public String getProjectID() {
        return projectID;
    }

    /**
     * @return The current version.
     */
    public String getCurrentVersion() {
        return currentVersion;
    }

    protected static String getPREFIX() {
        return PREFIX;
    }
}
