package de.imolli.glupdatechecker;

/**
 * @author Oliver Stahl
 * @version 1.0
 */
public class UpdateResult {

    public enum Status {
        NEW_VERSION_AVAILABLE,
        ALREADY_ON_NEWEST_VERSION,
        FAILED;
    }

    private Status status;
    private String newVersion;

    public UpdateResult(Status status, String newVersion) {
        this.status = status;
        this.newVersion = newVersion;
    }

    public Status getStatus() {
        return status;
    }

    public String getNewVersion() {
        return newVersion;
    }
}
