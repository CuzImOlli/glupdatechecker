# GLUpdateChecker

An API to check for updates via GitLab Tags.

## Usage:
```java
GLUpdateChecker glUpdateChecker = new GLUpdateChecker("10442455", "1.7.2");
UpdateResult result = glUpdateChecker.checkForUpdate();
```